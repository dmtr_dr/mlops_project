import logging
import pickle
import os
from pathlib import Path

import click
import pandas as pd
from dotenv import find_dotenv, load_dotenv
from sklearn import metrics
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import SGDClassifier
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.pipeline import Pipeline

import mlflow
from mlflow.models.signature import infer_signature
from mlflow.tracking import MlflowClient

load_dotenv()
remote_server_uri = os.getenv("MLFLOW_TRCKING_URI")
mlflow.set_tracking_uri(remote_server_uri)

@click.command()
@click.argument("input_filepath", type=click.Path(exists=True))
@click.argument("output_filepath", type=click.Path())
def main(input_filepath, output_filepath):
    mlflow.set_experiment("Text Classification Experiment")
    
    with mlflow.start_run() as run:
        df = pd.read_csv(input_filepath)

        X = df["processed_title"]
        y = df["Label"]

        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

        pipeline = Pipeline([
            ("tfidf", TfidfVectorizer()),
            ("rf_clf", RandomForestClassifier(random_state=42))
        ])

        pipeline.fit(X_train, y_train)
        y_pred = pipeline.predict(X_test)

        report = metrics.classification_report(y_test, y_pred, output_dict=True)
        mlflow.log_metrics({
            "precision": report["weighted avg"]["precision"],
            "recall": report["weighted avg"]["recall"],
            "f1-score": report["weighted avg"]["f1-score"]
        })
        
        print(metrics.classification_report(y_test, y_pred))

        signature = infer_signature(X_test, y_pred)
        mlflow.sklearn.log_model(pipeline, "random_forest_model", signature=signature)

        with open(output_filepath, "wb") as file:
            pickle.dump(pipeline, file)

        run_id = run.info.run_id
        click.echo(f"Run ID: {run_id}")

    client = MlflowClient()
    experiment = mlflow.get_experiment_by_name("Text Classification Experiment")
    experiment_id = experiment.experiment_id
    df = mlflow.search_runs([experiment_id])
    best_run_id = df.loc[0, 'run_id']
    print(best_run_id)

    logger = logging.getLogger(__name__)
    logger.info("making final data set from raw data")

if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    project_dir = Path(__file__).resolve().parents[2]
    load_dotenv(find_dotenv())
    main()