# -*- coding: utf-8 -*-
import click
import logging
from pathlib import Path
from dotenv import find_dotenv, load_dotenv
import pandas as pd
import nltk
import spacy
from nltk.corpus import stopwords
import subprocess
import sys


def download_spacy_model(model_name):
    try:
        spacy.load(model_name)
    except OSError:
        subprocess.run([sys.executable, "-m", "spacy", "download", model_name])

nltk.download('stopwords')
nltk.download('punkt')

download_spacy_model("ru_core_news_md")


@click.command()
@click.argument('input_filepath', type=click.Path(exists=True))
@click.argument('output_filepath', type=click.Path())
def main(input_filepath, output_filepath):
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    lenta = pd.read_csv(input_filepath)
    months_mapping = {
    'января': '01',
    'февраля': '02',
    'марта': '03',
    'апреля': '04',
    'мая': '05',
    'июня': '06',
    'июля': '07',
    'августа': '08',
    'сентября': '09',
    'октября': '10',
    'ноября': '11',
    'декабря': '12'
    }

    def replace_months(text):
        for russian_month, numeric_month in months_mapping.items():
            text = text.replace(russian_month, numeric_month)
        return text

    lenta['date'] = lenta['date'].apply(replace_months)
    lenta['date'] = lenta['date'].str[-10:]
    lenta['date'] = pd.to_datetime(lenta['date'], format='%d %m %Y', errors='coerce')
    lenta = lenta.drop(['Unnamed: 0'],axis = 1)

    stop_words = set(stopwords.words('russian'))
    nlp = spacy.load("ru_core_news_md")

    def preprocess_text(text):
        doc = nlp(text.lower()) 
        words = [token.lemma_ for token in doc if token.is_alpha and not token.is_stop and token.text not in stop_words]
        return ' '.join(words)
    
    lenta['processed_title'] = lenta['title'].apply(preprocess_text)
    lenta2 = lenta
    keywords = ['нефть', 'бензин', 'газ', 'энергоресурс', 'эксплорация', 'Буровая установка', 'месторожден', 'добыч', 'дизел', 'опек', 'танкеры']

    filtered_df = lenta2[lenta2['processed_title'].str.contains('|'.join(keywords), case=False, na=False)]
    filtered_df = filtered_df.drop(['title'],axis = 1)
    shares = ['BANE', 'BANEP', 'GAZP', 'LKOH', 'NVTK', 'ROSN', 'SIBN', 'SNGS', 'SNGSP', 'TATN', 'TATNP', 'YAKG']
    merged_data = pd.DataFrame()

    for i in shares:
        line = 'C:\\Users\\User1\\Desktop\\mlops_project\\data\\raw\\' + i + '_01012000_05012024.csv'
        df = pd.read_csv(line)
        df = df.drop(['<PER>','<TIME>','<TICKER>','<OPEN>','<HIGH>','<LOW>','<VOL>'], axis = 1)
        df = df.rename(columns={'<DATE>': 'DATE','<CLOSE>':'Close'})
        df['DATE'] = pd.to_datetime(df['DATE'], format='%Y%m%d')
        date_range = pd.date_range(start=df['DATE'].min(), end=df['DATE'].max(), freq='D')
        full_df = pd.DataFrame(index=date_range)
        result_df = pd.merge(full_df, df, left_index=True, right_on='DATE', how='left')
        result_df.Close = result_df.Close.fillna(method='ffill')
        result_df['Price_diff'] = (result_df['Close'].shift(-1) - result_df['Close'])/(result_df['Close'])
        result_df['Label'] = result_df['Price_diff'].apply(lambda x: 3 if x >= 0.02
                                                            else (2 if -0.02 < x < 0.02
                                                            else (1 if x <= -0.02
                                                            else 0)))

        result_df = pd.merge(filtered_df,result_df, how='left', left_on='date', right_on='DATE')
        result_df = result_df.dropna()
        result_df  = result_df .drop(['DATE', 'Price_diff', 'Close'], axis = 1)
        merged_data = pd.concat([merged_data, result_df])
    merged_data = merged_data.drop(['date'],axis = 1)
    merged_data = merged_data.groupby(['processed_title']).agg({'Label': lambda x: x.mode().iat[0] if not x.mode().empty else None}).reset_index()
    df1 = merged_data[merged_data['Label'] == 1]
    df1 = df1.loc[df1.index.repeat(12)].reset_index(drop=True)
    df3 = merged_data[merged_data['Label'] == 3]
    df3 = df3.loc[df3.index.repeat(15)].reset_index(drop=True)
    merged_data = pd.concat([merged_data[merged_data['Label'] == 2].reset_index(drop=True), df1, df3])
    merged_data.to_csv(output_filepath, index=False)

    logger = logging.getLogger(__name__)
    logger.info('making final data set from raw data')


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()
